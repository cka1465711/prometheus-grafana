minikube start --kubernetes-version=1.20.0 --driver=none
minikube start --driver=docker 
Le driver peut aussi être Virtualbox
minikube kubectl -- get pods -A
minikube addons enable metrics-server

1. Get the Odoo URL by running:

  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services odoo)
  export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  echo "Odoo URL: http://$NODE_IP:$NODE_PORT/"

2. Obtain the login credentials

  export ODOO_EMAIL=user@example.com
  export ODOO_PASSWORD=$(kubectl get secret --namespace "default" odoo -o jsonpath="{.data.odoo-password}" | base64 -d)

  echo Email   : $ODOO_EMAIL
  echo Password: $ODOO_PASSWORD

Souci qu'on rencontre dans ce projet ?  WSL2 est par défaut une VM en 'accès NAT' ==> Flux coupé dans un sens : on peut aller de la VM vers pas de Internet vers la VM.

NAT (Network Address Translation): Accession à Internet (si le pc hôte à accès à internet). Elles ont toutes la même adresse privée qui n'est pas sur le
réseau domestique. On peut communiquer avec TOUT sauf avec une autre VM configurée en NAT aussi car chaque VM est dans son propre sous-réseau virtuel. 
On ne peut pas non plus aller d'internet/hôte vers la VM à cause du principe de NAT lui même. Donc port fowarding.

```
minikube kubectl -- get service ou svc
minikube kubectl -- get namespace ou ns
minikube kubectl -- get replicaset ou rs
minikube kubectl -- get deployment ou deploy
minikube kubectl -- get nodes ou no
minikube kubectl -- get all -n loann
minikube kubectl -- get ingress
```
```
helm create <chartname>
helm lint <chartname>
helm list -A
helm install redisinsight redisin_loann/ -n loann --> redisinsight est le nom de la release/application
helm list -A --> redisinsight is now listed
helm upgrade redisinsight redisin_loann/ -n loann
helm uninstall redisinsight --> redisinsight now disappeared from list of deployed apps
helm repo list
helm repo add "..."
echo 'eval "$(helm completion bash)"' >> ~/.bashrc
echo "$(helm completion bash)" >> ~/.bashrc --> marche aussi je crois
source ~/.bashrc
helm list -A --> liste toutes les releases
helm uninstall redisinsight
helm uninstall redisinsight -n loann
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install my-ingress-controller ingress-nginx/ingress-nginx
minikube kubectl -- get po -n loann
minikube kubectl -- get ingress -n loann
```
